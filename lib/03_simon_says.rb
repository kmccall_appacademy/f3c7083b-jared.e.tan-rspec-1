def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, n = 2)
  times = 1
  new_word = []
  until times > n
    new_word << word
    times += 1
  end
  new_word.join(' ')
end

def start_of_word(word, n = 1)
  chars = word.chars
  new_word = []
  chars.each_with_index do |i, idx|
    new_word << i if idx < n
  end
  new_word.join
end

def first_word(sentence)
  words = sentence.split
  words.first
end

def titleize(sentence)
  words = sentence.split
  new_sentence = []
  words.each_with_index do |i, idx|
    if idx != 0 && i.length < 5 && i != "kwai"
      new_sentence << i
    else
      new_sentence << i.capitalize
    end
  end
  new_sentence.join(' ')
end
