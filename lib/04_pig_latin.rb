def translate(sentence)
  words = sentence.split
  latinized_words = words.map { |i| latinize(i) }
  latinized_words.join(' ')
end

def latinize(word)
  letters = word.chars
  consonants_to_add = []
  new_word = word.chars
  idx = 0
  until 'aeiou'.include?(letters[idx]) && 'q'.include?(letters[idx-1]) != true
    consonants_to_add << letters[idx]
    new_word.shift
    idx += 1
  end
  (new_word + consonants_to_add + ['ay']).join
end
