def add(num1,num2)
  sum = num1 + num2
end

def subtract(num1, num2)
  difference = num1 - num2
end

def sum(arr)
  sum = 0
  arr.each do |i|
    sum += i
  end
  sum
end

def multiply(arr)
  product = 1
  arr.each do |i|
    product *= i
  end
  product
end

def power(num1,num2)
  num1**num2
end

def factorial(num)

  new_num = 1
  factorial = 1
  until new_num > num
    factorial *= new_num
    new_num += 1 
  end
  factorial
end
